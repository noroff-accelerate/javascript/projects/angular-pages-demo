# Angular Demo on Gitlab Pages

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://noroff-accelerate.gitlab.io/javascript/projects/angular-pages-demo/)

Demo deployment of Angular to Gitlab Pages

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Most web applications expect to be deployed onto the root path of a web address, for example: `https://example.com/index.html`. With Gitlab Pages, this is not always possible and an adjustment needs to be made. The Angular toolchain has documentation about this [here](https://angular.io/guide/deployment#deploy-to-github-pages)

If you Git repository URL is this:

```
https://gitlab.com/yourusername/path/to/your/reponame
```

Then the resulting URL of the deployed Gitlab pages will be:

```
https://yourusername.gitlab.io/path/to/your/reponame
```

Therefore the Public Base Path that we must supply to the toolchain is: `/path/to/your/reponame`.

This repository is located at:

```
https://gitlab.com/noroff-accelerate/javascript/projects/angular-pages-demo
```

So the resulting deployment will be [here](https://noroff-accelerate.gitlab.io/javascript/projects/angular-pages-demo), and the resulting Public Base Path is: `/javascript/projects/angular-pages-demo`.

## Install

Make sure to have installed the Angular toolchain

```sh
npm install --global @angular/cli
```

Open a terminal or powershell window and run:

```sh
npm install
```

## Usage

Open a terminal or powershell window and run:

```sh
npm start
```

Further instructions will appear in your console. Leave the window open while in use.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Noroff Accelerate AS
