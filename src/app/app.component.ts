import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'demo';
  someNumber = -4;
  someArray = [42, 'foo'];

  get someNumberAbs() {
    return Math.abs(this.someNumber);
  }

  foobar() {
    return ['foo', 'bar'].join(' ');
  }

  asJSON(val: any) {
    return JSON.stringify(val);
  }
}
